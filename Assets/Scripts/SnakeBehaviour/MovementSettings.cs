﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewMovementSettings", menuName = "Movement/MovementSettings")]
public class MovementSettings : ScriptableObject
{
    [SerializeField] private float m_speed;
    [SerializeField] private int m_pointsRate;
    [SerializeField] private float m_timeRate;
    [SerializeField] private float m_rotationSpeed;
    [SerializeField] private float m_heightLimit;
    [SerializeField] private float m_widthLimit;

    public float Speed
    {
        get { return m_speed; }
        set { m_speed = value; }
    }

    public float TimeRate
    {
        get { return m_timeRate; }
        set { m_timeRate = value; }
    }

    public int PositionsRate
    {
        get { return m_pointsRate; }
        set { m_pointsRate = value; }
    }

    public float RotationSpeed
    {
        get { return m_rotationSpeed; }
        set { m_rotationSpeed = value; }
    }

    public float HeightLimit
    {
        get { return m_heightLimit; }
        set { m_heightLimit = value; }
    }
    public float WidthLimit
    {
        get { return m_widthLimit; }
        set { m_widthLimit = value; }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ASnakeMovement : MonoBehaviour
{
    [SerializeField] protected MovementSettings m_movementSettings;
    protected float m_desiredVelocity;
    protected List<Vector3> m_currentSavedPositions = new List<Vector3>();
    protected List<Quaternion> m_currentSavedRotations = new List<Quaternion>();
    protected List<Vector3> m_passedPositions = new List<Vector3>();
    protected List<Quaternion> m_passedRotations = new List<Quaternion>();

    protected abstract void Move();
    protected abstract void SaveCurrentCoordinates();

    protected bool IsExceedWidthLimit()
    {
        var xPosition = Mathf.Abs(transform.position.x);
        var widthLimit = Mathf.Abs(m_movementSettings.WidthLimit);
        return xPosition > widthLimit;
    }

    protected bool IsExceedHeightLimit()
    {
        var yPosition = Mathf.Abs(transform.position.y);
        var heightLimit = Mathf.Abs(m_movementSettings.HeightLimit);
        return yPosition > heightLimit;
    }
    
    public List<Vector3> GetCurrentSavedPositions()
    {
        return m_currentSavedPositions;
    }

    public List<Quaternion> GetCurrentSavedRotations()
    {
        return m_currentSavedRotations;
    }

    public float CalculateHalfDiameterSize()
    {
        return transform.GetComponent<BoxCollider2D>().bounds.size.x * Mathf.Sqrt(2f);
    }
}

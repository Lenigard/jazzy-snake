﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeBodyPartMovement : ASnakeMovement
{
    [SerializeField] private ASnakeMovement m_nextBodyPartScript;

    private int m_id;
    private int m_passedPositionsCount = 0;
    private List<Vector3> m_desiredPositions = new List<Vector3>();
    private List<Quaternion> m_desiredRotations = new List<Quaternion>();

    private const string HEAD_TAG = "Head";
    private const float DISTANCE_DELTA = 0.005f;
    private const float MOVEMENT_DELTA_EDGE = 1f;
    private float m_movementTimer = 0f;
    private float m_waiter = 0f;
    private float m_movementDelta = 0f;
    private float m_positionUpdateTime;

    private void Start()
    {
        m_positionUpdateTime = m_movementSettings.TimeRate / m_movementSettings.PositionsRate;
        m_id = this.transform.GetSiblingIndex();

        if (m_nextBodyPartScript == null)
        {
            if (m_id == 0)
            {
                m_nextBodyPartScript = GameObject.FindGameObjectWithTag(HEAD_TAG).GetComponent<ASnakeMovement>();
            }
            else
            {
                m_nextBodyPartScript = transform.parent.GetChild(m_id - 1).GetComponent<ASnakeMovement>();
            }
        }
    }

    private void Update()
    {
        Rotate();
        Move();
        UpdateDesiredCoordinates();
    }

    protected override void Move()
    {
        if (m_desiredPositions.Count != 0)
        {
            m_desiredVelocity = m_movementSettings.Speed * Time.fixedDeltaTime;
            float distance = Vector3.Distance(transform.position, m_desiredPositions[m_passedPositionsCount]);
            //m_movementDelta = m_movementTimer / m_movementSettings.TimeRate;

            if (distance < m_movementSettings.HeightLimit)
            {
                transform.position = Vector3.MoveTowards(transform.position, m_desiredPositions[0], m_desiredVelocity);
            }
            else
            {
                transform.position = m_desiredPositions[0];
            }
            if (distance < DISTANCE_DELTA)
            {
                m_movementTimer = 0f;
                SaveCurrentCoordinates();
            }
        }
        m_movementTimer += Time.deltaTime;
        //if (m_desiredPositions.Count != 0 && m_passedPositionsCount != m_desiredPositions.Count)
        //{
        //    float distance = Vector3.Distance(transform.position, m_desiredPositions[m_passedPositionsCount]);
        //    float movementDelta = m_timer / m_movementSettings.TimeRate;

        //    if (distance < m_movementSettings.HeightLimit)
        //    {
        //        transform.position = Vector3.MoveTowards(transform.position, m_desiredPositions[m_passedPositionsCount], m_desiredVelocity);
        //    }
        //    else
        //    {
        //        transform.position = m_desiredPositions[m_passedPositionsCount];
        //    }
        //    Debug.Log(m_id + "  " + m_passedPositionsCount);
        //    if (movementDelta > MOVEMENT_DELTA_EDGE || distance < DISTANCE_DELTA)
        //    {
        //        m_passedPositionsCount++;
        //        movementDelta = 0f;
        //        m_timer = 0f;
        //        if (m_passedPositionsCount == m_movementSettings.PositionsRate)
        //        {
        //            m_passedPositionsCount = 0;
        //        }
        //    }
        //}
    }

    private void Rotate()
    {
        if (m_desiredRotations.Count != 0)
        {
            Quaternion.Lerp(transform.rotation, m_desiredRotations[0], 1);
        }
    }

    protected override void SaveCurrentCoordinates()
    {
        //if (m_timer > m_movementSettings.TimeRate && m_passedPositionsCount == 0)
        //{
        //    if (m_desiredPositions.Count != 0)
        //    {
        //        m_currentSavedPositions = new List<Vector3>(m_desiredPositions);
        //    }
        //    m_desiredPositions = new List<Vector3>(m_nextBodyPartScript.GetCurrentSavedPositions());
        //}
        //m_timer += Time.deltaTime;
        //m_passedPositions.Add(m_desiredPositions[0]);
        //m_passedRotations.Add(m_desiredRotations[0]);
        m_passedPositions.Add(transform.position);
        m_passedRotations.Add(transform.rotation);
        m_desiredPositions.RemoveAt(0);
        m_desiredRotations.RemoveAt(0);
        if (m_passedPositions.Count == m_movementSettings.PositionsRate)
        {
            m_currentSavedPositions.Clear();
            m_currentSavedPositions.AddRange(m_passedPositions);
            m_passedPositions.Clear();
        }
        if (m_passedRotations.Count == m_movementSettings.PositionsRate)
        {
            m_currentSavedRotations.Clear();
            m_currentSavedRotations.AddRange(m_passedRotations);
            m_passedRotations.Clear();
        }
    }

    private void UpdateDesiredCoordinates()
    {
        if (m_waiter > m_movementSettings.TimeRate)
        {
            if (m_desiredPositions.Count == 0 && m_nextBodyPartScript.GetCurrentSavedPositions().Count == m_movementSettings.PositionsRate)
            {
                m_desiredPositions = new List<Vector3>(m_nextBodyPartScript.GetCurrentSavedPositions());
                m_desiredRotations = new List<Quaternion>(m_nextBodyPartScript.GetCurrentSavedRotations());
            }
        }
        else
        {
            m_waiter += Time.deltaTime;
        }
    }
}

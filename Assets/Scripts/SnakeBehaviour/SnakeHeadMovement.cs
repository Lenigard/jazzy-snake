﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeHeadMovement : ASnakeMovement
{
    [SerializeField] private int m_rotationAngle = 90;
    [SerializeField] private GameObject prefab;

    private float m_timer;
    private float m_positionUpdateTime;
    private Boolean m_isPositionExceed;

    private void Start()
    {
        m_timer = Time.time;
        m_positionUpdateTime = m_movementSettings.TimeRate / m_movementSettings.PositionsRate;
    }

    // Update is called once per frame
    private void Update()
    {
        Move();
        Rotate();
        SaveCurrentCoordinates();
    }

    protected override void Move()
    {
        m_desiredVelocity = m_movementSettings.Speed * Time.fixedDeltaTime;
        Boolean isExceedHeigtLimit = IsExceedHeightLimit();
        Boolean isExceedWidthLimit = IsExceedWidthLimit();
        transform.Translate(0, m_desiredVelocity, 0, Space.Self);
        if(!m_isPositionExceed && isExceedHeigtLimit)
        {
            transform.position = new Vector3(transform.position.x, -transform.position.y, transform.position.z);
            m_isPositionExceed = true;
        }
        if (!m_isPositionExceed && isExceedWidthLimit)
        {
            transform.position = new Vector3(-transform.position.x, transform.position.y, transform.position.z);
            m_isPositionExceed = true;
        }
        if(!isExceedWidthLimit && !isExceedHeigtLimit)
        {
            m_isPositionExceed = false;
        }
    }

    private void Rotate()
    {
        float desiredZAngle = 0f;
        float deltaRotation = Time.deltaTime * m_rotationAngle * m_movementSettings.RotationSpeed;
        if (Input.GetKey(KeyCode.RightArrow))
        {
            desiredZAngle = -deltaRotation;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            desiredZAngle = deltaRotation;
        }
        transform.Rotate(0, 0, desiredZAngle);
    }

    protected override void SaveCurrentCoordinates()
    {        
        if (m_timer < Time.time)
        {
            m_timer += m_positionUpdateTime;
            Instantiate(prefab, transform.position, transform.rotation);
            m_passedPositions.Add(transform.position);
            m_passedRotations.Add(transform.rotation);
            if (m_passedPositions.Count == m_movementSettings.PositionsRate)
            {
                m_currentSavedPositions.Clear();
                m_currentSavedPositions.AddRange(m_passedPositions);
                m_passedPositions.Clear();
            }
            if (m_passedRotations.Count == m_movementSettings.PositionsRate)
            {
                m_currentSavedRotations.Clear();
                m_currentSavedRotations.AddRange(m_passedRotations);
                m_passedRotations.Clear();
            }
        }
    }
}
